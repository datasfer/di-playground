﻿using Microsoft.Owin.Cors;
using Owin;
using OwinSelfHost.DI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace OwinSelfHost
{
    public class WebApiStartup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            appBuilder.Map("/api", api =>
            {
                var config = new HttpConfiguration();
                config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{controller}/{name}/{id}",
                defaults: new { id = RouteParameter.Optional }
                     );

                // Add IoC
                var serviceProvider = IocStartup.BuildServiceProvider();
                config.DependencyResolver = new DefaultDependencyResolver(serviceProvider);

                // ...
                api.UseCors(CorsOptions.AllowAll);
                api.UseWebApi(config);
            });
        }
    }
}
