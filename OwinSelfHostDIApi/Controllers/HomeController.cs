﻿using CommonBits.Services;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace OwinSelfHost.Controllers
{
    [Route("home")]
    public class HomeController : ApiController
    {
        private readonly INumberService _service;
        public HomeController(INumberService service)
        {
            _service = service;
        }
       
        [HttpGet]
        [Route("num")]
        public HttpResponseMessage number()
        {
           // return Json(new { num = _service.GetNumber() });
            return this.Request.CreateResponse(
                        HttpStatusCode.OK,
                        new { Message = "Random numer action", Value = _service.GetNumber() });
                        }

       
    }
}