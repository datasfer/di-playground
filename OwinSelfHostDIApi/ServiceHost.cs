﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OwinSelfHost
{
    public class ServiceHost
    {
        private IDisposable server = null;
        const string baseAddress = "http://localhost:8001";

        public void Start()
        {
            server = WebApp.Start<WebApiStartup>(baseAddress);
            Console.WriteLine($"Web Server is running at {baseAddress}");
        }
    }
}
