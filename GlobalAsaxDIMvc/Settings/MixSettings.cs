﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GenericHostIntegration.Settings
{
    // https://andrewlock.net/how-to-use-the-ioptions-pattern-for-configuration-in-asp-net-core-rc2/
    public class MixSettings
    {
        public string StringSetting { get; set; }
        public int IntSetting { get; set; }
        public Dictionary<string, InnerClass> Dict { get; set; }
        public List<string> ListOfValues { get; set; }
        public MyEnum AnEnum { get; set; }

    }

    public class InnerClass
    {
        public string Name { get; set; }
        public bool IsEnabled { get; set; } = true;
    }

    public enum MyEnum
    {
        None = 0,
        Lots = 1
    }

}