﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GenericHostIntegration.Settings
{
    public class AppSettings
    {
       public string  DefaultConnection { get; set; }
       public string ProductionConnection { get; set; }
    }
}