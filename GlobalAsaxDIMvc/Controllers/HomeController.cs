﻿using CommonBits.Services;
using GenericHostIntegration.Settings;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GenericHostIntegration.Controllers
{
    public class HomeController : Controller
    {
        private readonly INumberService _service;
        private readonly ILogger<HomeController> _logger;
        private AppSettings AppSettings { get; set; }
        private MixSettings MixSettings { get; set; }

        private IConfiguration Configuration { get; set; }

        public HomeController( INumberService service, 
                               ILogger<HomeController> logger,
                               IOptions<AppSettings> appSettings,
                               IOptions<MixSettings> mixSettings,
                               IConfiguration configuration)
        {
            _service = service;
            _logger = logger;
            AppSettings = appSettings.Value;
            MixSettings = mixSettings.Value;

            Configuration = configuration;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            var randomNumber = _service.GetNumber();
            var configField = Configuration.GetValue<string>("ConnectionStrings:ProductionConnection");
            ViewBag.Message = $@"Random Number Service Result: {randomNumber} - {AppSettings.DefaultConnection} - {configField}
                                {MixSettings.AnEnum} - {MixSettings.IntSetting}
                              ";
            _logger.LogInformation($"Service called, result: {randomNumber}");
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}