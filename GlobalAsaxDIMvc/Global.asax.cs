﻿using CommonBits.DI;
using CommonBits.Extensions;
using CommonBits.Services;
using GenericHostIntegration;
using GenericHostIntegration.Controllers;
using GenericHostIntegration.Settings;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;


namespace GenericHostIntegration
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            IServiceCollection services = new ServiceCollection().AddLogging();
            ConfigureServices(services);
           
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);



            ConfigureServices(services);
            services.AddOptions();
            services.AddConfiguration<AppSettings>(@"App_Data\config.json", "AppSettings");
            services.AddConfiguration<MixSettings>(@"App_Data\config.json", "MixSettings");


            var resolver = new DefaultDependencyResolver(services);
            DependencyResolver.SetResolver(resolver);
        }

        private void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<INumberService, RandomNumberService>();
            services.AddLogging();

            services.AddControllersAsServices(typeof(MvcApplication).Assembly.GetExportedTypes()
                    .Where(t => !t.IsAbstract && !t.IsGenericTypeDefinition)
                    .Where(t => typeof(IController).IsAssignableFrom(t)
                        || t.Name.EndsWith("Controller", StringComparison.OrdinalIgnoreCase)));   
           // services.AddTransient<HomeController>();
        }
    }



    //public class MvcApplication : System.Web.HttpApplication
    //{
    //    public static void InitModule()
    //    {
    //        RegisterModule(typeof(ServiceScopeModule));
    //    }

    //    protected void Application_Start()
    //    {
    //        IServiceCollection services = new ServiceCollection().AddLogging();
    //        ConfigureServices(services);
    //        ServiceScopeModule.SetServiceProvider(services.BuildServiceProvider());

    //        AreaRegistration.RegisterAllAreas();
    //        FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
    //        RouteConfig.RegisterRoutes(RouteTable.Routes);
    //        BundleConfig.RegisterBundles(BundleTable.Bundles);

    //        ConfigureServices(services);
    //        var resolver = new DefaultDependencyResolver(services);
    //        DependencyResolver.SetResolver(resolver);

    //        //DependencyResolver.SetResolver(new ServiceProviderDependencyResolver());
    //    }

    //    private void ConfigureServices(IServiceCollection services)
    //    {
    //      //  services.AddScoped<ScopedThing>();
    //        services.AddScoped<INumberService, RandomNumberService>();
    //        services.AddTransient<HomeController>();

    //    }
    //}

    //public class ScopedThing : IDisposable
    //{
    //    public ScopedThing()
    //    {

    //    }
    //    public void Dispose()
    //    {
    //    }
    //}

    //internal class ServiceScopeModule : IHttpModule
    //{
    //    private static ServiceProvider _serviceProvider;

    //    public void Dispose()
    //    {

    //    }

    //    public void Init(HttpApplication context)
    //    {
    //        if (context == null)
    //        {
    //            throw new ArgumentNullException("context");
    //        }
    //        context.BeginRequest += Context_BeginRequest;
    //        context.EndRequest += Context_EndRequest;
    //    }

    //    private void Context_EndRequest(object sender, EventArgs e)
    //    {
    //        var context = ((HttpApplication)sender).Context;
    //        if (context.Items[typeof(IServiceScope)] is IServiceScope scope)
    //        {
    //            scope.Dispose();
    //        }
    //    }

    //    private void Context_BeginRequest(object sender, EventArgs e)
    //    {
    //        var context = ((HttpApplication)sender).Context;
    //        context.Items[typeof(IServiceScope)] = _serviceProvider.CreateScope();
    //    }

    //    public static void SetServiceProvider(ServiceProvider serviceProvider)
    //    {
    //        _serviceProvider = serviceProvider;
    //    }
    //}

    //internal class ServiceProviderDependencyResolver : IDependencyResolver
    //{
    //    public object GetService(Type serviceType)
    //    {
    //        if (HttpContext.Current?.Items[typeof(IServiceScope)] is IServiceScope scope)
    //        {
    //            return scope.ServiceProvider.GetService(serviceType);
    //        }

    //        throw new InvalidOperationException("IServiceScope not provided");
    //    }

    //    public IEnumerable<object> GetServices(Type serviceType)
    //    {
    //        if (HttpContext.Current?.Items[typeof(IServiceScope)] is IServiceScope scope)
    //        {
    //            return scope.ServiceProvider.GetServices(serviceType);
    //        }

    //        throw new InvalidOperationException("IServiceScope not provided");
    //    }
    //}

    //public class Startup
    //{
    //    IConfigurationRoot Config { get; }

    //    public Startup()
    //    {
    //        var builder = new ConfigurationBuilder()
    //            .AddJsonFile("appsettings.json");

    //        Config = builder.Build();
    //    }

    //    public void ConfigureServices(IServiceCollection services)
    //    {
    //        services.AddLogging();
    //        services.AddSingleton<IConfigurationRoot>(Config);
    //    }
    //}
    //public class MvcApplication : System.Web.HttpApplication
    //{
    //    //private IHost _host;
    //    //public MvcApplication()
    //    //{
    //    //    _host = new HostBuilder()
    //    //      .ConfigureServices((hostContext, services) =>
    //    //      {
    //    //          services.AddScoped<INumberService, RandomNumberService>();
    //    //      }).Build();
    //    //}

    //    protected void Application_Start()
    //    {
    //       // _host.Start();

    //        AreaRegistration.RegisterAllAreas();
    //        FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
    //        RouteConfig.RegisterRoutes(RouteTable.Routes);
    //        BundleConfig.RegisterBundles(BundleTable.Bundles);


    //    }

    //    //public override void Dispose()
    //    //{
    //    //    using (_host)
    //    //    {
    //    //        _host.StopAsync(TimeSpan.FromSeconds(5)).Wait();
    //    //    }
    //    //}
    //}
}
