﻿using CommonBits.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OwinStartupDiPlayground.Controllers
{
    public class HomeController : Controller
    {
        private readonly INumberService _service;
        public HomeController(INumberService service)
        {
            _service = service;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = $"Random Number Service Result: {_service.GetNumber()}";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}