﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.Extensions.DependencyInjection;
using System.Web.Mvc;
using CommonBits.Services;
using CommonBits.DI;

[assembly: OwinStartup(typeof(OwinStartupDiPlayground.Startup))]

namespace OwinStartupDiPlayground
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var services = new ServiceCollection();
            ConfigureServices(services);
            var resolver = new DefaultDependencyResolver(services);
            DependencyResolver.SetResolver(resolver);
        }

        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<INumberService, RandomNumberService>();
            services.AddTransient(typeof(Controllers.HomeController));
            
        }
    }
}
