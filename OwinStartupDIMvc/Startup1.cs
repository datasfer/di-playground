﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using CommonBits.DI;
using CommonBits.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(OwinStartupDiPlayground.Startup1))]

namespace OwinStartupDiPlayground
{
    public class Startup1
    {
        public void Configuration(IAppBuilder appBuilder)
        {

            var services = new ServiceCollection();
            ConfigureServices(services);
            var resolver = new DefaultDependencyResolver(services);
            DependencyResolver.SetResolver(resolver);
        }

        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<INumberService, RandomNumberService>();
            services.AddTransient(typeof(Controllers.HomeController));
        }
    }
}
