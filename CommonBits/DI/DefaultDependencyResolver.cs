﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CommonBits.DI
{
    public class DefaultDependencyResolver : IDependencyResolver
    {
        protected IServiceProvider ServiceProvider;

        public DefaultDependencyResolver(IServiceCollection serviceProvider)
        {
            if (serviceProvider == null)
            {
                throw new ArgumentNullException(nameof(serviceProvider));
            }
            // defualt implementation url: https://github.com/aspnet/Mvc/blob/48546dbb28ee762014f49caf052dc9c8a01eec3a/src/Microsoft.AspNetCore.Mvc.Core/DependencyInjection/MvcCoreServiceCollectionExtensions.cs#L159
            // has a cache, so need to be singleon
            serviceProvider.AddSingleton<IControllerFactory, ScopedControllerFactory>(_ => new ScopedControllerFactory(this));

            ServiceProvider = serviceProvider.BuildServiceProvider();
        }

        public object GetService(Type serviceType)
        {
            var service = ServiceProvider.GetService(serviceType);
            return service;
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return ServiceProvider.GetServices(serviceType);
        }

        class ScopedControllerFactory : DefaultControllerFactory
        {
            private DefaultDependencyResolver parentResolver;

            public ScopedControllerFactory(DefaultDependencyResolver defaultDependencyResolver)
            {
                if (defaultDependencyResolver == null)
                {
                    throw new ArgumentNullException(nameof(defaultDependencyResolver));
                }
                this.parentResolver = defaultDependencyResolver;
            }

            protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
            {

                if (controllerType == null)
                {
                    return base.GetControllerInstance(requestContext, controllerType);
                }

                using (var scope = this.parentResolver.ServiceProvider.CreateScope())
                {
                    requestContext.HttpContext.Items[typeof(IServiceScope)] = scope;
                   //    requestContext.HttpContext.Items["DiScope"] = scope;
                    return (IController)scope.ServiceProvider.GetService(controllerType);
                }
            }
            public override void ReleaseController(IController controller)
            {
                var mvcController = controller as Controller;

                var context = mvcController.HttpContext;
                if (context.Items[typeof(IServiceScope)] is IServiceScope scope)
                {
                    scope.Dispose();
                }


               
                //if (mvcController != null)
                //{
                //    var scope = (IServiceScope)mvcController.HttpContext.Items["DiScope"];
                //    scope.Dispose();
                //}

                base.ReleaseController(controller);
            }
        }
    }
}