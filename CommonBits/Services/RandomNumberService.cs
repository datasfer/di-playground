﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace CommonBits.Services
{
    public class RandomNumberService : INumberService, IDisposable
    {
        public void Dispose()
        {
            Debug.WriteLine("Test scoped service dispose call: RandomNumberService disposed");
        }

        public int GetNumber()
        {
           var rand = new Random();
            return rand.Next(1, 100);
        }
    }
}