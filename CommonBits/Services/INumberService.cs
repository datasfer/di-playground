﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonBits.Services
{
    public interface INumberService
    {
        int GetNumber();
    }
}