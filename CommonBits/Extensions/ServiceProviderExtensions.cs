﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonBits.Extensions
{
    public static class ServiceProviderExtensions
    {
        public static IServiceCollection AddControllersAsServices(this IServiceCollection services,
           IEnumerable<Type> controllerTypes)
        {
            foreach (var type in controllerTypes)
            {
                services.AddTransient(type);
            }

            return services;
        }


        public static IServiceCollection AddConfiguration<T>(this IServiceCollection services, string jsonFile, string section)
            where T : class
        {
            var builder = new ConfigurationBuilder()
               .AddJsonFile(jsonFile);
            var configuration = builder.Build();
            services.Configure<T>(configuration.GetSection(section));
            
            // this line provide access to generic IConfiguration
            services.AddSingleton<IConfiguration>(configuration);
            return services;
        }
    }
}